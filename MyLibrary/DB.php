<?php

namespace MyLibrary;

class DB
{

    private $connection;

    public function __construct()
    {
        $connection = mysqli_connect("127.0.0.1", "root", "", "zenclass");
        if (!$connection) {
            echo "Ошибка: Невозможно установить соединение с MySQL." . PHP_EOL;
            echo "Код ошибки errno: " . mysqli_connect_errno() . PHP_EOL;
            echo "Текст ошибки error: " . mysqli_connect_error() . PHP_EOL;
            exit;
        }
        $this->connection = $connection;
    }

    public function insert($query): bool
    {
        $result = mysqli_query($this->connection, $query);
        if (!$result) {
            throw new \Exception();
        }
        return $result;
    }

    public function query($query): array
    {
        $result = mysqli_query($this->connection, $query);
        if (!$result) {
            throw new \Exception();
        }
        $res = [];
        while ($row = mysqli_fetch_assoc($result)) {
            $res[] = $row;
        }
        return $res;
    }
}
