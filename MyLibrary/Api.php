<?php

namespace MyLibrary;

class Api
{

    public static function jsonResponse($status, $payload, $message = '')
    {
        $resArr['status'] = $status ? 'ok' : 'error';
        $resArr['payload'] = $payload;
        $message ? $resArr['message'] = $message : false;
        return json_encode($resArr);
    }
}
