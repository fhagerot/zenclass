<?php

namespace Controllers;

use Models\Participant;
use Models\Session;
use MyLibrary\Api;

class SessionController
{

    public function subscribe()
    {
        $sessionId = (int) $_GET['sessionId'];
        $userEmail = (string) $_GET['userEmail'];
        $session = Session::factory()->getSession($sessionId);
        if (!isset($session[0])) {
            return Api::jsonResponse(false, [], 'Сессия не найдена');
        }
        $paricipant = Participant::factory()->getUserByEmail($userEmail);
        if (!isset($paricipant[0])) {
            return Api::jsonResponse(false, [], 'Пользователь не найден');
        }
        if (Session::factory()->freePlaces($sessionId)) {
            try {
                Session::factory()->subscribe($paricipant[0]['ID'], $sessionId);
                return Api::jsonResponse(true, [], 'Спасибо, вы успешно записаны!');
            } catch (\Exception $e) {
                return Api::jsonResponse(false, [], 'Вы уже записаны');
            }
        }
        return Api::jsonResponse(false, [], 'Извините, все места заняты');
    }
}
