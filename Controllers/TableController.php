<?php

namespace Controllers;

use Models\AvailableTables;
use Models\Table;
use MyLibrary\Api;

class TableController
{

    public function getTableData()
    {
        print_r($_REQUEST);
        if (AvailableTables::factory()->checkAvailable((string) $_REQUEST['table'])) {
            return Api::jsonResponse(true, Table::factory()->getDataByTable((int) $_REQUEST['id'], (string) $_REQUEST['table']));
        }
        return Api::jsonResponse(false, [], 'Таблица не доступна');
    }
}
