<?php

namespace Models;

use MyLibrary\Model;
use Traits\Factory;

class AvailableTables extends Model
{
    use Factory;

    public function checkAvailable(string $tableName): bool
    {
        $sql = "SELECT * from `available_tables` where `tbl`='{$tableName}'";
        $res = $this->db->query($sql);
        return isset($res[0]);
    }
}
