<?php

namespace Models;

use MyLibrary\Model;
use Traits\Factory;

class Participant extends Model
{
    use Factory;

    public function getUserByEmail(string $email): array
    {
        $sql = "SELECT * from `participant` where `email`='{$email}' LIMIT 1";
        return $this->db->query($sql);
    }
}
