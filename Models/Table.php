<?php

namespace Models;

use MyLibrary\Model;
use Traits\Factory;

class Table extends Model
{
    use Factory;

    public function getDataByTable(int $id = null, string $tableName): array
    {
        $sql = "SELECT * from `{$tableName}`";
        if ($id) {
            $sql .=  " where `id`={$id}";
        }
        return $this->db->query($sql);
    }
}
