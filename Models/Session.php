<?php

namespace Models;

use MyLibrary\Model;
use Traits\Factory;

class Session extends Model
{
    use Factory;

    public function getSession(int $sessionId): array
    {
        $sql = "SELECT * from `session` where `ID`='{$sessionId}' LIMIT 1";
        return $this->db->query($sql);
    }

    public function freePlaces(int $sessionId): bool
    {
        $sql = "SELECT count(*) as NUM FROM `participant_sessions` WHERE `id_session`={$sessionId} GROUP BY `id_session`";
        $placed = $this->db->query($sql);
        $sql = "SELECT `max_places` FROM `session` WHERE `ID`={$sessionId}";
        $maxPlaces = $this->db->query($sql);
        return $maxPlaces[0]['max_places'] > $placed[0]['NUM'];
    }

    public function subscribe(int $userId, int $sessionId): bool
    {
        $sql = "INSERT INTO `participant_sessions`(`id_participant`,`id_session`) VALUES ({$userId}, $sessionId);";
        return $this->db->insert($sql);
    }
}
