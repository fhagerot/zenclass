<?php

namespace Traits;

trait Factory
{

    public static function factory()
    {
        return new self();
    }
}
