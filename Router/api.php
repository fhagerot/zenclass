<?php

use Phroute\Phroute\RouteCollector;
use Phroute\Phroute\Dispatcher;

function processInput($uri)
{
    $uri = urldecode(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
    return $uri;
}

function processOutput($response)
{
    echo json_encode($response);
}

$router = new RouteCollector();

$router->group(['prefix' => 'api'], function (RouteCollector $router) {
    $router->post('SessionSubscribe', [Controllers\SessionController::class, 'subscribe']);
    $router->post('Table', [Controllers\TableController::class, 'getTableData']);
});

$dispatcher =  new Dispatcher($router->getData());

try {
    $response = $dispatcher->dispatch($_SERVER['REQUEST_METHOD'], processInput($_SERVER['REQUEST_URI']));
} catch (Phroute\Exception\HttpRouteNotFoundException $e) {
    var_dump($e);
    die();
} catch (Phroute\Exception\HttpMethodNotAllowedException $e) {
    var_dump($e);
    die();
}

processOutput($response);
